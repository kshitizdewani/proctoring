from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Staff(models.Model):
    name=models.CharField(max_length=60)
    designation=models.CharField(max_length=40)
    department=models.CharField(max_length=30)

    def __str__(self):
        return self.name+" | "+self.department

class Tutor(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    picture=models.ImageField(upload_to='tutors',default='default.jpeg')

    def __str__(self): return (self.user.first_name + " " + self.user.last_name)

class Course(models.Model):
    tutor=models.ForeignKey(Tutor,models.SET_NULL,blank=True,null=True)
    title=models.CharField(max_length=150,blank=True,null=True)

    def __str__(self):return self.title

class Batch(models.Model):
    course=models.ForeignKey(Course,on_delete=models.CASCADE,null=False,blank=False)
    name=models.CharField(max_length=100,null=True,blank=True)

    def __str__(self): return (self.name+f'({self.course.title})')



class Student(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE)
    course=models.ForeignKey(Course,models.SET_NULL,null=True,blank=True)
    batch=models.ForeignKey(Batch,models.SET_NULL,null=True,blank=True)
    picture=models.ImageField(upload_to='students',default='default.jpeg',)
    contact=models.PositiveBigIntegerField()
    address=models.CharField(max_length=500,null=True,blank=True)

    def __str__(self): return (self.user.first_name + ' ' + self.user.last_name)

class Notice(models.Model):
    sender=models.ForeignKey(User,on_delete=models.CASCADE)
    batch=models.ForeignKey(Batch,on_delete=models.CASCADE,null=True,blank=True)
    course=models.ForeignKey(Course,on_delete=models.CASCADE,null=True,blank=True)
    student=models.ForeignKey(Student,on_delete=models.CASCADE,null=True,blank=True)
    body=models.CharField(max_length=1000)
    attachment=models.FileField(upload_to='media/notice_attachments',null=True,blank=True)

class Mediafile(models.Model):
    attachment=models.FileField(upload_to='media')
