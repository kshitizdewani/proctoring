from django.urls import path,include
from rest_framework import routers
from .views import *
from rest_framework_simplejwt import views as jwt_views
# from .serialzers import StaffSerializer

# router = routers.DefaultRouter()
# router.register('',StaffViewSet,basename='staff_list')
# router.register('',NoticeViewSet,basename='notices_list')

urlpatterns = [
    # path('staff/',include(router.urls)),
    path('notices/',NoticesList.as_view()),
    path('tutors/',TutorList.as_view()),
    path('students/',StudentList.as_view()),
    path('batches/',BatchesList.as_view()),
    path('batches/course=<int:courseID>/',BatchFilterByCourse.as_view()),
    path('courses/',CoursesList.as_view()),
    # path('token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    # path('token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]
