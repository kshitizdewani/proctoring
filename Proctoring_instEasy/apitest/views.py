from django.http.multipartparser import MultiPartParser
from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponse
from .models import Staff
# from tutor_dashboard.models import Notice
from rest_framework import viewsets, generics, status
from .serialzers import *
from rest_framework.permissions import AllowAny

# Create your views here.

#Just for testing
class StaffViewSet(viewsets.ModelViewSet):
    queryset = Staff.objects.all()
    serializer_class = StaffSerializer

class BatchesList(generics.ListCreateAPIView):
    queryset = Batch.objects.all()
    serializer_class = BatchesSerializer
    permission_classes = [AllowAny]

class BatchFilterByCourse(APIView):
    def get(self,request,courseID,format=None):
        """
        This view should return a list of all the batches
        for a given course.
        """
        course = Course.objects.get(id=courseID)
        queryset= Batch.objects.filter(course=course)
        serializer = BatchesSerializer(queryset, many=True)
        return Response(serializer.data)

class CoursesList(generics.ListCreateAPIView):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
    permission_classes = [AllowAny]

class NoticesList(generics.ListCreateAPIView):
    queryset = Notice.objects.all()
    # parser_classes = (MultiPartParser,)
    serializer_class = NoticeSerializer
    permission_classes = [AllowAny]

    def post(self,request, *args, **kwargs):
        sender = User.objects.get(id=request.data['sender'])
        if not request.data['course'] == None:
            course = Course.objects.get(id=request.data['course'])
        else:
            course = request.data['course']

        if not request.data['batch'] == None:
            Batch = Batch.objects.get(id=request.data['batch'])
        else:
            batch = request.data['batch']

        student = request.data['student']
        body = request.data['body']
        attachment = request.data['attachment']
        print(request.data,request.FILES)
        Notice.objects.create(sender=sender,course=course,batch=batch,student=student,body=body,attachment=attachment)
        print(f'sender: {sender}\n course: {course}\n attachment: {attachment}')
        return

class TutorList(generics.ListCreateAPIView):
    queryset = Tutor.objects.all()
    serializer_class = TutorSerializer
    permission_classes = [AllowAny]

class StudentList(generics.ListCreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    permission_classes = [AllowAny]

# class BatchesList(generics.ListCreateAPIView):
#     queryset = Batch.objects.all()
#     serializer_class = Ba

class current_user(APIView):

    permission_classes = (AllowAny,)

    def get(self, request, format=None):
      serializer = UserSerializer(request.user)
      return Response(serializer.data)

class UserList(APIView):

    permission_classes = (AllowAny,)
    # http_method_names = ['get', 'head','post']

    def get(self, request, format=None):
        users = User.objects.all()
        serializer = UserSerializerWithToken(users, many=True)
        return Response(serializer.data)

    def post(self, request, format=None):
        serializer = UserSerializerWithToken(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
