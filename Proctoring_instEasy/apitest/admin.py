from django.contrib import admin
from .models import *
# Register your models here.

# admin.site.register(Staff)
admin.site.register(Tutor)
admin.site.register(Student)
admin.site.register(Batch)
admin.site.register(Course)
admin.site.register(Notice)
