from rest_framework import serializers
from apitest.models import Staff
from .models import *



class StaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Staff
        fields = (
            "id",
            "name",
            "designation",
            "department",
            # "url"
        )
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('__all__')

    def create(self, validated_data):
        groups_data = validated_data.pop('groups')
        user = User.objects.create(**validated_data)
        for group_data in groups_data:
            # Group.objects.create(user=user, **group_data)
            user.groups.add(group_data)
        return user

class BatchesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Batch
        fields = ("id","course","name")

class NoticeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notice
        fields = ('id',"sender",'course','batch','student','body','attachment')


class TutorSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)
    class Meta:
        model = Tutor
        fields = ("__all__")

class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ("__all__")

class StudentSerializer(serializers.ModelSerializer):
    # user = UserSerializer(read_only=True)

    class Meta:
        model = Student
        fields = ("__all__")


# class UserSerializerWithToken(serializers.ModelSerializer):
#
#     token = serializers.SerializerMethodField()
#     password = serializers.CharField(write_only=True)
#     # usergroup = serializers.CharField(read_only=True)
#
#     def get_token(self, obj):
#         jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
#         jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
#
#         payload = jwt_payload_handler(obj)
#         token = jwt_encode_handler(payload)
#         return token
#
#     def create(self, validated_data):
#         password = validated_data.pop('password', None)
#         instance = self.Meta.model(**validated_data)
#         if password is not None:
#             instance.set_password(password)
#         instance.save()
#
#         return instance
#
#     class Meta:
#         model = User
#         fields = ('token', 'username', 'password','groups')
