from django.apps import AppConfig


class TutotDashboardConfig(AppConfig):
    name = 'tutot_dashboard'
