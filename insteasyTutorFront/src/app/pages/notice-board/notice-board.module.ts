import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { NoticeBoardPageRoutingModule } from './notice-board-routing.module';
import { NoticeBoardPage } from './notice-board.page';
import { MaterialModule } from "../../material.module";
import { AngularEditorModule } from '@kolkov/angular-editor';
import { AppModule } from "../../app.module";
import {MatTableComponent} from "../../components/mat-table/mat-table.component";
import {DataPropertyGetterPipePipe} from "../../components/mat-table/data-property-getter-pipe.pipe";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NoticeBoardPageRoutingModule,
    MaterialModule,
    AngularEditorModule,
    // AppModule
    // MatTableComponent
  ],
  exports: [DataPropertyGetterPipePipe ],
  declarations: [NoticeBoardPage, MatTableComponent, DataPropertyGetterPipePipe, ]

})
export class NoticeBoardPageModule {}
