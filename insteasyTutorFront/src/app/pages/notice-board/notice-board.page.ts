/* tslint:disable:no-trailing-whitespace */
import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {NoticeService} from '../../services/notice/notice.service';
import {Batch, Course, Notice, TableColumn} from '../../interfaces';
import {BatchesService} from '../../services/batches/batches.service';
import {CoursesService} from '../../services/courses/courses.service';
import {Observable} from 'rxjs';
import {AngularEditorConfig} from '@kolkov/angular-editor';

@Component({
  selector: 'app-notice-board',
  templateUrl: './notice-board.page.html',
  styleUrls: ['./notice-board.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NoticeBoardPage implements OnInit {
  public notices = [];
  public batches = [];
  public courses = [];
  public textdata = '';
  public  selectedBatch = null;
  public selectedCourse = null;
  newpost: Observable<Notice>;
  ordersTableColumns: TableColumn[];
  selectedFile: File = null;

  constructor(private _noticeservice: NoticeService,
              private _batchservice: BatchesService,
              private _courseservices: CoursesService,
              private cd: ChangeDetectorRef) {
    // console.log(this.notices);
  }

  ngOnInit() {
    this._noticeservice.getNotices()
        .subscribe(data => {
            this.notices = data;
            this.cd.detectChanges();
          }
        );
    this._batchservice.getBatches()
        .subscribe(x => this.batches = x);
    this._courseservices.getCourses()
        .subscribe(y => this.courses = y);
    console.log(this.notices);
    this.initializeColumns();
  }


   onFileChange(event) {
    this.selectedFile = (event.target.files[0] as File);
    console.log('got the file.');
    console.log(this.selectedFile);
  }

  createNotice() {
    console.log('..createNotice() running');
    console.log(this.selectedFile);
    this._noticeservice
    .postNotice({
        sender: 1 ,
        course: this.selectedCourse,
        batch: this.selectedBatch,
        student: null,
        body: this.textdata,
        attachment: this.selectedFile,
    })
    .subscribe(item => this.notices.push(item));
    this._noticeservice.getNotices()
    .subscribe(data => {
      this.notices = data;
      this.cd.detectChanges();
    }
    );
  }


  // ------Table Columns----------
  initializeColumns(): void {
    this.ordersTableColumns = [
      {
        name: 'Sender',
        dataKey: 'sender',
        position: 'left',
        isSortable: false
      },
      {
        name: 'Course',
        dataKey: 'course',
        position: 'right',
        isSortable: false
      },
      {
        name: 'Batch',
        dataKey: 'batch',
        position: 'right',
        isSortable: false
      },
      {
        name: 'Student',
        dataKey: 'student',
        position: 'right',
        isSortable: false
      },
      {
        name: 'Announcement',
        dataKey: 'body',
        position: 'right',
        isSortable: false
      },
    ];
  }


// ------------Text Editor-----------
  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: '345px',
      minHeight: '50',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'no',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      defaultFontName: '',
      defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
  };

}
