import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    {
    path: 'notice-board',
    loadChildren: () => import('./pages/notice-board/notice-board.module').then( m => m.NoticeBoardPageModule)
  },
  {
    path: '',
    redirectTo: 'notice-board',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'overview',
    loadChildren: () => import('./pages/overview/overview.module').then( m => m.OverviewPageModule)
  },
  {
    path: 'noticeboard-archive',
    loadChildren: () => import('./pages/noticeboard-archive/noticeboard-archive.module').then( m => m.NoticeboardArchivePageModule)
  },  {
    path: 'student-assignment-report',
    loadChildren: () => import('./pages/student-assignment-report/student-assignment-report.module').then( m => m.StudentAssignmentReportPageModule)
  },
  {
    path: 'create-assignment',
    loadChildren: () => import('./pages/create-assignment/create-assignment.module').then( m => m.CreateAssignmentPageModule)
  },



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
