import { Component } from '@angular/core';
import { UserService } from './services/user/user.service';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { ActionSheetController } from '@ionic/angular';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public actionbuttons = [];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private userService: UserService,
    private route: Router,
    public actionSheetController: ActionSheetController,

  ) {
    // -----------Checking if user is logged in---------
    if (localStorage.getItem('user')){
      this.actionbuttons = [{
        text: 'Logout',
        role: 'destructive',
        icon: 'log-out-outline',
        handler: () => {
          this.onLogout();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }];
      console.log('Logged in from memory');
      const user = JSON.parse(localStorage.getItem('user'));
      const token = user.token;
      // username = user.token.username;
    }
    else {
      // ------- adding login button to action sheet --------
      this.actionbuttons = [{
        text: 'Login',
        role: 'close',
        icon: 'log-in-outline',
        handler: () => {
          this.route.navigate(['/login']);
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }];
      // ----------Redirecting to login if User is not authenticated ---------------
      // this.route.navigate(['/login']);
    }
  }
  onLogout(){
    this.userService.logout();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

//  ------- Action sheet --------
  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Actions',
      cssClass: 'my-custom-class',
      buttons: this.actionbuttons
    });
    await actionSheet.present();
  }

}
