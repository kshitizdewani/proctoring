from django.shortcuts import render
from .models import SelectGroup
from .serializers import SelectGroupSerializer
from rest_framework import viewsets

class SelectGroupViewSet(viewsets.ModelViewSet):
    queryset = SelectGroup.objects.all()
    serializer_class = SelectGroupSerializer