from rest_framework import serializers
from .models import SelectGroup


class SelectGroupSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','title', 'studentsub', 'batch', 'course')
        model = SelectGroup