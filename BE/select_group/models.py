from django.db import models
from student_sub.models import StudentSubjectTaken
from batch.models import Batch
from course.models import Course

class SelectGroup(models.Model):
    title = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    studentsub = models.ForeignKey(StudentSubjectTaken,on_delete=models.CASCADE,null=True,blank=True)
    batch = models.ForeignKey(Batch,on_delete=models.CASCADE,null=True,blank=True)
    course = models.ForeignKey(Course,on_delete=models.CASCADE,null=True,blank=True)

    def __str__(self):
        return self.title