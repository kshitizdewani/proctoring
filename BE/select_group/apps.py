from django.apps import AppConfig


class SelectGroupConfig(AppConfig):
    name = 'select_group'
