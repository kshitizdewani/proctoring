from django.db import models
# from chapter.models import Chapter

class Syllabus(models.Model):
    title = models.CharField(max_length=200)
    start_date = models.DateField(blank=True)
    end_date = models.DateField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    # chapter = models.ForeignKey(Chapter,on_delete=models.CASCADE,null=True,blank=True)

    def __str__(self):
        return self.title

    def getChapters(self):
        from chapter.models import Chapter
        chapters_list = Chapter.objects.filter(syllabus=self)
        return list(chapters_list)