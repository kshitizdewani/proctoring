from django.shortcuts import render
from .models import Syllabus
from .serializers import SyllabusSerializer
from rest_framework import viewsets
from rest_framework.views import APIView
import json
from django.http import HttpResponse

class SyllabusViewSet(APIView):
    def get(self,request,syllabusID,format=None):
        syllabus_object=Syllabus.objects.get(id=syllabusID)
        # chapterslist
        x={
            "id":syllabus_object.id,
            "name":syllabus_object.title,
            "chapters": [
                {"name":i.name,
                "subtopics":[j.title for j in i.getSubtopics()]  
                } for i in syllabus_object.getChapters()
            ],
        }
        # print(x)
        return HttpResponse(json.dumps(x))
        
