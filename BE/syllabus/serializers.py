from rest_framework import serializers
from .models import Syllabus


class SyllabusSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','title','start_date','end_date')
        model = Syllabus