from rest_framework import serializers
from .models import Subtopic


class SubtopicSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','title','comment','attachment','chapter')
        model = Subtopic