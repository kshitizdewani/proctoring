from django.apps import AppConfig


class SubTopicConfig(AppConfig):
    name = 'sub_topic'
