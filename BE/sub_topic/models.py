from django.db import models
from chapter.models import Chapter

class Subtopic(models.Model):
    title = models.CharField(max_length=200)
    comment = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    attachment = models.FileField(upload_to='subtopic',null=True) 
    chapter = models.ForeignKey(Chapter,on_delete=models.CASCADE,null=True,blank=True)

    def __str__(self):
        return self.title