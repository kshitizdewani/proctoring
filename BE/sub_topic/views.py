from django.shortcuts import render
from .models import Subtopic
from .serializers import SubtopicSerializer
from rest_framework import viewsets

class SubtopicViewSet(viewsets.ModelViewSet):
    queryset = Subtopic.objects.all()
    serializer_class = SubtopicSerializer