from django.db import models
from teacher_details.models import TeacherDetail

class Course(models.Model):
    
    name = models.CharField(max_length=200)
    start_date = models.DateField(blank=True)
    end_date = models.DateField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    tutor = models.ForeignKey(TeacherDetail,on_delete=models.CASCADE)


    def __str__(self):
        return self.name
