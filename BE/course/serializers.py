from rest_framework import serializers
from .models import Course


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','name','start_date','end_date')
        model = Course