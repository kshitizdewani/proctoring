
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
# <<<<<<< HEAD


from batch.views import BatchViewSet
# =======
from batch.views import BatchViewSet,BatchFilterByCourse
# >>>>>>> 05da443fa1a89af4af8a0de8202dbdbbb430b67b/
from chapter.views import ChapterViewSet
from course.views import CourseViewSet
from notice_board.views import NoticeBoardViewSet
from select_group.views import SelectGroupViewSet
from student_details.views import StudentDetailViewSet
from student_sub.views import StudentSubjectTakenViewSet
from sub_topic.views import SubtopicViewSet
from subject.views import SubjectViewSet
from syllabus.views import SyllabusViewSet
from teacher_details.views import TeacherDetailViewSet
from teacher_sub.views import TeacherSubjectTakenViewSet
from django.conf.urls.static import static
from . import settings

router = routers.DefaultRouter()
router.register(r'batch', BatchViewSet)
router.register(r'chapters', ChapterViewSet)
router.register(r'courses', CourseViewSet)
router.register(r'noticeboard', NoticeBoardViewSet)
router.register(r'selectgroup', SelectGroupViewSet)
router.register(r'studentdetails', StudentDetailViewSet)
router.register(r'studentsub',StudentSubjectTakenViewSet)
router.register(r'subtopic', SubtopicViewSet)
router.register(r'subject', SubjectViewSet)
# router.register(r'syllabus', SyllabusViewSet)
router.register(r'teacherdetails', TeacherDetailViewSet)
router.register(r'teachersub',TeacherSubjectTakenViewSet)


urlpatterns = [
    path('api/', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls')),
# <<<<<<< HEAD
    # path('api/batch?course=<int:course>/',BatchFilterByCourse.as_view()),
    # path('api/batches/',BatchViewSet.as_view({'get': 'list'})),
# =======
    path('batches/course=<int:courseID>/',BatchFilterByCourse.as_view()),
    path('api/batches/',BatchViewSet.as_view({'get': 'list'})),
    path('api/syllabus/<int:syllabusID>/',SyllabusViewSet.as_view() )
# >>>>>>> 05da443fa1a89af4af8a0de8202dbdbbb430b67b
    # batch/course=4/

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
