from django.db import models
from django.contrib.auth.models import User


class StudentDetail(models.Model):
    
    name = models.CharField(max_length=200)
    contactno = models.IntegerField(null=True)
    address = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    photo = models.ImageField(upload_to='studentphoto',null=True)

    def __str__(self):
        return self.name