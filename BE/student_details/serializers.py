from rest_framework import serializers
from .models import StudentDetail


class StudentDetailSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','name','contactno','address','owner','photo')
        model = StudentDetail