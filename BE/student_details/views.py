from django.shortcuts import render
from .models import StudentDetail
from .serializers import StudentDetailSerializer
from rest_framework import viewsets

class StudentDetailViewSet(viewsets.ModelViewSet):
    queryset = StudentDetail.objects.all()
    serializer_class = StudentDetailSerializer