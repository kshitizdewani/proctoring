from django.apps import AppConfig


class AttachFileConfig(AppConfig):
    name = 'attach_file'
