from django.db import models
from syllabus.models import Syllabus
from batch.models import Batch

class Subject(models.Model):
    name = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    syllabus = models.ForeignKey(Syllabus,on_delete=models.CASCADE,null=True,blank=True)
    batch = models.ForeignKey(Batch,on_delete=models.CASCADE,null=True,blank=True)

    def __str__(self):
        return self.name