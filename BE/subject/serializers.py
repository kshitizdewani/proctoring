from rest_framework import serializers
from .models import Subject


class SubjectSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','name','syllabus','batch')
        model = Subject