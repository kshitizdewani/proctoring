from django.db import models
from course.models import *

class Batch(models.Model):
    
    name = models.CharField(max_length=200)
    start_date = models.DateField(blank=True)
    end_date = models.DateField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    Course = models.ForeignKey(Course,on_delete=models.CASCADE)

    def __str__(self):
        return self.name
