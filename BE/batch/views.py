from django.shortcuts import render
from .models import Batch
from course.models import Course
from .serializers import BatchSerializer
from course.serializers import CourseSerializer
from rest_framework import viewsets,generics
# <<<<<<< HEAD
from rest_framework.decorators import action
# =======
from rest_framework.views import APIView
from rest_framework.response import Response
# >>>>>>> 05da443fa1a89af4af8a0de8202dbdbbb430b67b

class BatchViewSet(viewsets.ModelViewSet):
    queryset = Batch.objects.all()
    serializer_class = BatchSerializer

# <<<<<<< HEAD
    # def get_queryset(self):
    #     id = self.request.query_params.get('id', None)

    #     if id:
    #         batchid = Batch.objects.filter(id=id)
    #     return batchid

# class BatchFilterByCourse(generics.ListAPIView):
#     serializer_class = BatchSerializer
#     def get_queryset(self):
#         course = Course.objects.get(id=self.request.course)
#         print('get queryset')
#         return Batch.objects.filter(course=course)
# =======
class BatchFilterByCourse(APIView):
    def get(self,request,courseID,format=None):
        """
        This view should return a list of all the batches
        for a given course.
        """
        course = Course.objects.get(id=courseID)
        queryset= Batch.objects.filter(course=course)
        serializer = BatchSerializer(queryset, many=True)
        return Response(serializer.data)
# >>>>>>> 05da443fa1a89af4af8a0de8202dbdbbb430b67b
