from rest_framework import serializers
from .models import Batch


class BatchSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','name','start_date','end_date','Course')
        model = Batch