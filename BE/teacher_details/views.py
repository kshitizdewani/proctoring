from django.shortcuts import render
from .models import TeacherDetail
from .serializers import TeacherDetailSerializer
from rest_framework import viewsets

class TeacherDetailViewSet(viewsets.ModelViewSet):
    queryset = TeacherDetail.objects.all()
    serializer_class = TeacherDetailSerializer