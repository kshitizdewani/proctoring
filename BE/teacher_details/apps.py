from django.apps import AppConfig


class TeacherDetailsConfig(AppConfig):
    name = 'teacher_details'
