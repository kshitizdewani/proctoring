from rest_framework import serializers
from .models import TeacherDetail


class TeacherDetailSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','name','contactno','address','owner','photo')
        model = TeacherDetail