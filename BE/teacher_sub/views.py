from django.shortcuts import render
from .models import TeacherSubjectTaken
from .serializers import TeacherSubjectTakenSerializer
from rest_framework import viewsets

class TeacherSubjectTakenViewSet(viewsets.ModelViewSet):
    queryset = TeacherSubjectTaken.objects.all()
    serializer_class = TeacherSubjectTakenSerializer