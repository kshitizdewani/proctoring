from django.apps import AppConfig


class TeacherSubConfig(AppConfig):
    name = 'teacher_sub'
