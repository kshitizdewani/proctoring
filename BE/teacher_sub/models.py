from django.db import models
from teacher_details.models import TeacherDetail
from subject.models import Subject

class TeacherSubjectTaken(models.Model):
    name = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    teacherdetail = models.ForeignKey(TeacherDetail, on_delete=models.CASCADE) 
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE) 

    def __str__(self):
        return self.name