from rest_framework import serializers
from .models import TeacherSubjectTaken


class TeacherSubjectTakenSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','name','teacherdetail','subject')
        model = TeacherSubjectTaken