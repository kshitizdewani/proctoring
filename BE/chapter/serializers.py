from rest_framework import serializers
from .models import Chapter


class ChapterSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','name','overview','start_date','end_date','syllabus')
        model = Chapter