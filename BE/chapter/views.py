from django.shortcuts import render
from .models import Chapter
from .serializers import ChapterSerializer
from rest_framework import viewsets

class ChapterViewSet(viewsets.ModelViewSet):
    queryset = Chapter.objects.all()
    serializer_class = ChapterSerializer
