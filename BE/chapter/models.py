from django.db import models
from syllabus.models import Syllabus


class Chapter(models.Model):
    name = models.CharField(max_length=200)
    overview = models.TextField()
    start_date = models.DateField(blank=True)
    end_date = models.DateField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    syllabus= models.ForeignKey(Syllabus,on_delete=models.CASCADE,null=True,blank=True)

    def __str__(self):
        return self.name

    def getSubtopics(self):
        from sub_topic.models import Subtopic
        allsubtopics= Subtopic.objects.filter(chapter=self)
        return list(allsubtopics)


    
