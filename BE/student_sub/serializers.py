from rest_framework import serializers
from .models import StudentSubjectTaken


class StudentSubjectTakenSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','name','studentdetail','subject')
        model = StudentSubjectTaken