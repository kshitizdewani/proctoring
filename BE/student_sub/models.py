from django.db import models
from student_details.models import StudentDetail
from subject.models import Subject

class StudentSubjectTaken(models.Model):
    name = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    studentdetail = models.ForeignKey(StudentDetail, on_delete=models.CASCADE) 
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE) 

    def __str__(self):
        return self.name