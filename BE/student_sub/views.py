from django.shortcuts import render
from .models import StudentSubjectTaken
from .serializers import StudentSubjectTakenSerializer
from rest_framework import viewsets

class StudentSubjectTakenViewSet(viewsets.ModelViewSet):
    queryset = StudentSubjectTaken.objects.all()
    serializer_class = StudentSubjectTakenSerializer