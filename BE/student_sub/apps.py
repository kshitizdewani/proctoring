from django.apps import AppConfig


class StudentSubConfig(AppConfig):
    name = 'student_sub'
