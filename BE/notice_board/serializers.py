from rest_framework import serializers
from .models import NoticeBoard


class NoticeBoardSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id','title','date','announcement','teacher', 'attachment','course','batch')
        model = NoticeBoard