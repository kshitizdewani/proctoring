from django.shortcuts import render
from .models import NoticeBoard
from .serializers import NoticeBoardSerializer
from rest_framework import viewsets

class NoticeBoardViewSet(viewsets.ModelViewSet):
    queryset = NoticeBoard.objects.all()
    serializer_class = NoticeBoardSerializer
