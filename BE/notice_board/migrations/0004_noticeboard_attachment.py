# Generated by Django 3.1.3 on 2020-12-04 14:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notice_board', '0003_noticeboard_teacher'),
    ]

    operations = [
        migrations.AddField(
            model_name='noticeboard',
            name='attachment',
            field=models.FileField(default=1, upload_to='notice_files'),
            preserve_default=False,
        ),
    ]
