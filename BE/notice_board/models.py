from django.db import models
from teacher_details.models import TeacherDetail
from course.models import Course
from batch.models import Batch


class NoticeBoard(models.Model):

    title = models.CharField(max_length=200)
    date = models.DateField(null=True)
    announcement = models.TextField()
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    teacher = models.ForeignKey(TeacherDetail,on_delete=models.CASCADE)
    attachment = models.FileField(upload_to='notice_files',null=True) 
    course = models.ForeignKey(Course,on_delete=models.CASCADE)
    batch = models.ForeignKey(Batch,on_delete=models.CASCADE,null=True,blank=True)

    def __str__(self):
        return self.title
